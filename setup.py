#!/usr/bin/env python

from setuptools import setup
from media_link import __version__ as version

setup(name="media-link",
      version=version,
      description="Tool for auto-discovery and linking of media files.",
      author="Matt Reay",
      author_email="tmattreay@gmail.com",
      packages=["media_link"])
