#pylint: disable=C0103,R0204,R0903

import os
import platform
import logging
import importlib
import fileinput
import datetime
from collections import namedtuple
from enum import Enum

import dateutil.parser

LOGGER = logging.getLogger()


class OperatingSystem(Enum):
    Unix = 0
    Windows = 1
    Unknown = -1

class SymlinkLog(object):
    """ Singleton Symlink Log File """

    __instance = None

    def __new__(cls):
        create_instance = SymlinkLog.__instance is None or \
                          not isinstance(SymlinkLog.__instance, SymlinkLog)
        if create_instance:
            SymlinkLog.__instance = super().__new__(cls)
        return SymlinkLog.__instance

    def __init__(self):
        self.os = None
        self.wintool = None
        self.__setOS()
        self.userdir = os.path.expanduser("~")
        self.symlink_log = None
        self.__setSymlinkLog()

    @staticmethod
    def __touch(file_path):
        """ Verify that a file exists """
        try:
            os.utime(file_path, None)
        except FileNotFoundError:
            LOGGER.info("Making new file: %s", file_path)
            open(file_path, "a", encoding="utf-8").close()

    def __setOS(self):
        os_type = platform.system()
        if os_type == "Linux":
            LOGGER.info("OS Type is Linux")
            self.os = OperatingSystem.Unix
        elif os_type == "Windows":
            LOGGER.info("OS Type is Windows")
            self.os = OperatingSystem.Windows
            self.wintool = importlib.import_module("ctypes.windll.kernel32")
        else:
            LOGGER.info("OS Type is unknown")
            self.os = OperatingSystem.Unknown

    def __setSymlinkLog(self):
        config_dir = self.__makeConfigDirectory()
        self.symlink_log = os.path.join(config_dir, "symlink-log")
        self.__touch(self.symlink_log)

    def __makeConfigDirectory(self):
        """ Make directory to log symlinks """
        base_config_dir = os.path.join(self.userdir, ".config")
        config_dir = os.path.join(base_config_dir, "media-link")
        if not os.path.exists(base_config_dir):
            LOGGER.info("Making new config directory: %s", base_config_dir)
            os.mkdir(base_config_dir)
            if self.os == OperatingSystem.Windows:
                self.wintool.SetFileAttributesW(base_config_dir, 2)
        if not os.path.exists(config_dir):
            LOGGER.info("Making new config directory: %s", config_dir)
            os.mkdir(config_dir)
        return config_dir

    def getLinks(self):
        """ Retrieve all logged links """
        Link = namedtuple("Link", ["date", "target", "link"])
        links = []
        with open(self.symlink_log, "r", encoding="utf-8") as log_file:
            for line in log_file.readlines():
                date, target, link = line.strip("\n").split()
                date = dateutil.parser.parse(date)
                links.append(Link(date, target, link))
        return links

    def logLink(self, target, link):
        """ Log a symbolic link """
        date = datetime.datetime.now()
        log_line = "{:<30} {} {}\n".format(date.isoformat(), target, link)
        with open(self.symlink_log, "a", encoding="utf-8") as log_file:
            log_file.write(log_line)
        return 0

    def unlogLink(self, link):
        """ Unlog a specific link """
        return_code = -1
        with fileinput.input(self.symlink_log, inplace=True) as log_file:
            for line in log_file:
                _, _, line_link = line.strip("\n").split()
                if os.path.abspath(link) == os.path.abspath(line_link):
                    return_code = 0
                    continue
                print(line, end="")
        return return_code
