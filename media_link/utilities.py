#pylint: disable=C0103,R0204,E0401,C0326

import os
import logging

from .objects import SymlinkLog

LOGGER = logging.getLogger()
SYMLINK_LOG = SymlinkLog()


def __makeSymlink(target, link):
    """ Make symbolic link based on OS """
    if not os.path.exists(target):
        LOGGER.exception("Target not found: %s", target)
        return_code = -1
    else:
        try:
            is_directory = os.path.isdir(target)
            os.symlink(target, link, target_is_directory=is_directory)
            return_code = 0
        except FileNotFoundError:
            LOGGER.exception("Link not valid: %s", link)
            return_code = -1
        except NotImplementedError:
            LOGGER.exception("Windows version invalid")
            return_code = -1
        except OSError:
            LOGGER.exception("User cannot make symlink")
            return_code = -1
    return return_code

def linkDir(target, link):
    """ Link a given directory and log it """
    return_code = __makeSymlink(target, link)
    if return_code == 0:
        SYMLINK_LOG.logLink(target, link)
    return return_code

def unlinkDir(link):
    """ Delete link and unlog it """
    if not os.path.exists(link):
        return_code = -1
    else:
        return_code = SYMLINK_LOG.unlogLink(link)
        if return_code != 0:
            LOGGER.error("Failed to unlog link: %s", link)
        else:
            try:
                os.remove(link)
            except OSError:
                LOGGER.error("Failed to remove link: %s", link)
                return_code = -1
    return return_code

def getLinkPaths():
    """ Retrieve file paths to all logged symlinks """
    return [ link.link for link in SYMLINK_LOG.getLinks() ]

def unlinkAllDirs():
    links = getLinkPaths()
    return_code = 0
    for link in links:
        if unlinkDir(link) != 0:
            return_code = -1
    return return_code
